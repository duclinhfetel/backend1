FROM python:3.8

# python buffer
ENV PYTHONUNBUFFERED=1

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY app /app

ENTRYPOINT [ "python","main.py" ]
# CMD python manage.py 
